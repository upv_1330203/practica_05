package course.examples.practica_05;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;

public class MainActivity extends AppCompatActivity {

    public final static String EXTRA_MESSAGE = "";
    public static Intent intent = null;

    Button button;
    Button button1;
    Button button2;
    Button button3;
    ImageView image;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        addListenerOnButton();
    }

    public void addListenerOnButton(){

        button = (Button)findViewById(R.id.btnChangeImage);
        button1 = (Button)findViewById(R.id.btnChangeImage1);
        button2 = (Button)findViewById(R.id.btnChangeImage2);
        button3 = (Button)findViewById(R.id.btnChangeImage3);

        intent = new Intent(this, Main2Activity.class);

        button.setOnClickListener(new View.OnClickListener(){

            @Override
            public void onClick(View arg0){
                intent.putExtra(EXTRA_MESSAGE, "0");
                startActivity(intent);
            }
        });

        button1.setOnClickListener(new View.OnClickListener(){

            @Override
            public void onClick(View arg0){
                intent.putExtra(EXTRA_MESSAGE, "1");
                startActivity(intent);
            }
        });

        button2.setOnClickListener(new View.OnClickListener(){

            @Override
            public void onClick(View arg0){
                intent.putExtra(EXTRA_MESSAGE, "2");
                startActivity(intent);
            }
        });

        button3.setOnClickListener(new View.OnClickListener(){

            @Override
            public void onClick(View arg0){
                intent.putExtra(EXTRA_MESSAGE, "3");
                startActivity(intent);
            }
        });
    }
}
