package course.examples.practica_05;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;

public class Main2Activity extends AppCompatActivity {
    ImageView image;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);

        Intent intent = getIntent();
        String message = intent.getStringExtra(MainActivity.EXTRA_MESSAGE);
        int nImage = Integer.parseInt(message);

        image = (ImageView) findViewById(R.id.imageView1);

        if(nImage==0){
            image.setImageResource(R.drawable.manzana);
        }else if(nImage==1){
            image.setImageResource(R.drawable.uva);
        }else if(nImage==2){
            image.setImageResource(R.drawable.sandia);
        }else if(nImage==3){
            image.setImageResource(R.drawable.pina);
        }
    }
}
